package main

import (
	"log"
	"net/http"

	"C1_Video_Call/server"
)

func main() {
	//calls init function
	server.AllRooms.Init()

	//endpoints
	http.HandleFunc("/create", server.CreateRoomHandler)
	http.HandleFunc("/join", server.JoinRoomHandler)

	log.Println("Starting Service on Port 8080")
	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		log.Fatal(err)
	}

}
