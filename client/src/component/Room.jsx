import React, { useEffect, useRef } from "react";
import html2canvas from "html2canvas";

let mediaRecorder; //API bawaan JS untuk mengambil data stream dari media device yang aktif
let chunks = []; // stream data yang akan dibeca selama recording yang nanti akan dibaca setelah dilemparkan ke element video

const Room = (props) => {
    const userVideo = useRef();
    const userStream = useRef();
    const partnerVideo = useRef();
    const peerRef = useRef();
    const webSocketRef = useRef();

    

    const openCamera = async () => {
        //you can find all available media device by entering
        //navigator.mediaDevices.enumerateDevices() to console.log()
        const allDevices = await navigator.mediaDevices.enumerateDevices();
        const cameras = allDevices.filter(
            (device) => device.kind == "videoinput"
        );

        //constraint data for camera and audio device of users
        const constraints = {
            audio: {
                deviceId: allDevices[2].deviceId
            },
            video: {
                deviceId: cameras[1].deviceId
            },
        };

        //sets the video and audio used for the call with data from the constraints
        try {
            return await navigator.mediaDevices.getUserMedia(constraints);
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        openCamera().then((stream) => {
            //starts the user camera
            userVideo.current.srcObject = stream;
            userStream.current = stream;

            //validates/joins user into the room via websocket
            webSocketRef.current = new WebSocket(
                `ws://localhost:8080/join?room_id=${props.match.params.roomID}`
            );
        
        //only runs theses functions when webSocket successfully connected
        if(webSocketRef.current){
            webSocketRef.current.addEventListener("open", () => {
                webSocketRef.current.send(JSON.stringify({ join: true }));
            });

            webSocketRef.current.addEventListener("message", async (e) => {
                const message = JSON.parse(e.data);

                if (message.join) {
                    callUser();
                }

				if (message.offer) {
                    handleOffer(message.offer);
                }

                if (message.answer) {
                    console.log("Receiving Answer");
                    peerRef.current.setRemoteDescription(
                        new RTCSessionDescription(message.answer)
                    );
                }

                if (message.iceCandidate) {
                    console.log("Receiving and Adding ICE Candidate");
                    try {
                        await peerRef.current.addIceCandidate(
                            message.iceCandidate
                        );
                    } catch (err) {
                        console.log("Error Receiving ICE Candidate", err);
                    }
                }
            });
        }
        });
    });


    //handles received offer by returning answer to the server

    const handleOffer = async (offer) => {
        console.log("Received Offer, Creating Answer");
        peerRef.current = createPeer();

        await peerRef.current.setRemoteDescription(
            new RTCSessionDescription(offer)
        );

        userStream.current.getTracks().forEach((track) => {
            peerRef.current.addTrack(track, userStream.current);
        });

        const answer = await peerRef.current.createAnswer();
        await peerRef.current.setLocalDescription(answer);

        webSocketRef.current.send(
            JSON.stringify({ answer: peerRef.current.localDescription })
        );
    };

    const callUser = () => {
        console.log("Waiting the other User...");
        peerRef.current = createPeer();

        userStream.current.getTracks().forEach((track) => {
            peerRef.current.addTrack(track, userStream.current);
        });
    };

    //Establish p2p connection using a google STUN server and ICE Candidates

    const createPeer = () => {
        console.log("Initiating Peer Connection");
        const peer = new RTCPeerConnection({
            iceServers: [
            {url:'stun:stun1.l.google.com:19302'},
            {url:'stun:stun2.l.google.com:19302'},
            {url:'stun:stun3.l.google.com:19302'},
            {url:'stun:stun4.l.google.com:19302'},],
        });

        peer.onnegotiationneeded = handleNegotiationNeeded;
        peer.onicecandidate = handleIceCandidateEvent;
        peer.ontrack = handleTrackEvent;

        return peer;
    };

    //Creates an Offer message to the server

    const handleNegotiationNeeded = async () => {
        console.log("Creating Offer");

        try {
            const myOffer = await peerRef.current.createOffer();
            await peerRef.current.setLocalDescription(myOffer);

            webSocketRef.current.send(
                JSON.stringify({ offer: peerRef.current.localDescription })
            );
        } catch (err) {}
    };

    const handleIceCandidateEvent = (e) => {
        console.log("Found Ice Candidate");
        if (e.candidate) {
            console.log(e.candidate);
            webSocketRef.current.send(
                JSON.stringify({ iceCandidate: e.candidate })
            );
        }
    };

    const handleTrackEvent = (e) => {
        console.log("Received Tracks");
        partnerVideo.current.srcObject = e.streams[0];
    };


    //fungsi/handler untuk screenshot
    const imgCapture = (e) => {
        e.preventDefault()
        html2canvas(document.querySelector("#capture")).then(canvas => {
            document.body.appendChild(canvas)
        })
        console.log("Capture Successful");
    }


    //handler untuk memulai recording
    const startRecordingHandler = () => {
        mediaRecorder = new MediaRecorder(userVideo.current.srcObject);
        mediaRecorder.ondataavailable = e => {
        chunks.push(e.data);
        };
        mediaRecorder.onstop = e => {
        const recordedBlob = new Blob(chunks, { type: "video/mp4" });
        const recordedUrl = URL.createObjectURL(recordedBlob);
        const videoElement = document.getElementById("myVideo");
        videoElement.src = recordedUrl;
        console.log(document.getElementById("myVideo"));
    };
        mediaRecorder.start();
    };

    //handler untuk stop recording
    const stopRecordingHandler = () => {
        mediaRecorder.stop();
    };

    return (
        <div id="capture"> 
            <video autoPlay controls={true} ref={userVideo}></video>
            <video autoPlay controls={true} ref={partnerVideo}></video>
            <video controls={true} id="myVideo"></video>
            <button onClick={startRecordingHandler}>Start</button>
            <button onClick={stopRecordingHandler}>Stop</button>
            <button onClick={imgCapture}>Capture Room Image</button>
            <canvas id="imgCanvas"></canvas>
        </div>
    );
};

export default Room;
