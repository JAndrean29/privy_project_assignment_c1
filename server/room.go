package server

import (
	"log"
	"math/rand"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type User struct {
	Host bool
	Conn *websocket.Conn
}

// Map of Call Rooms
type RoomMap struct {
	Mutex sync.RWMutex
	Map   map[string][]User
}

// Initialize Map
func (r *RoomMap) Init() {
	r.Map = make(map[string][]User)
}

// Get Specific Room ID
func (r *RoomMap) Get(roomID string) []User {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	return r.Map[roomID]
}

// Create a Room
func (r *RoomMap) CreateRoom() string {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	rand.Seed(time.Now().UnixNano())
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	b := make([]rune, 12)

	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	roomID := string(b)

	r.Map[roomID] = []User{}

	return roomID
}

// Servers moves user to designated room_id
func (r *RoomMap) InsertToRoom(roomID string, host bool, conn *websocket.Conn) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	u := User{host, conn}

	log.Printf("Inserting user %v to room: %v", u, roomID)
	r.Map[roomID] = append(r.Map[roomID], u)
}

// Delete Room
func (r *RoomMap) DeleteRoom(roomID string) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	delete(r.Map, roomID)
}
