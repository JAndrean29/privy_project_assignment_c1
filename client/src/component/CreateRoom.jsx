import React from "react";

const CreateRoom = (props) => {
    const create = async (e) => {
        //prevent page from refreshing on submit
        e.preventDefault()

        //get room id data from "create" endpoint
        const resp = await fetch("http://localhost:8080/create")
        const{room_id} = await resp.json()
        
        //pushes /room/${room_id} to url and access the url
        props.history.push(`/room/${room_id}`)
    }

    //create room button
    return (
        <div>
            <button onClick={create}>Create Room</button>
        </div>
    )
}

export default CreateRoom