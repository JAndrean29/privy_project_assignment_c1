package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// Server global hashmap
var AllRooms RoomMap

// Create room and returns Room ID
func CreateRoomHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	roomID := AllRooms.CreateRoom()

	fmt.Printf("\nRoom Created, %v\n", roomID)

	type response struct {
		RoomID string `json:"room_id"`
	}

	json.NewEncoder(w).Encode(response{RoomID: roomID})
}

// upgrade connection to websocket
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// pass a broadcast message to a room(s)
type broadcastMsg struct {
	Message map[string]interface{}
	RoomID  string
	Client  *websocket.Conn
}

// Creates channel to pass a broadcastMsg to another go routine
var broadcast = make(chan broadcastMsg)

func broadcaster() {
	for {
		msg := <-broadcast

		//checks if certain client is still in the room,if a client disconnects connection will be terminated
		for _, client := range AllRooms.Map[msg.RoomID] {
			if client.Conn != msg.Client {
				err := client.Conn.WriteJSON(msg.Message)

				if err != nil {
					log.Fatal("Error: ", err)
					client.Conn.Close()
				}
			}
		}
	}
}

// join request handler, later calls insert to room function to add user to the room
func JoinRoomHandler(w http.ResponseWriter, r *http.Request) {
	roomID, ok := r.URL.Query()["room_id"]

	if !ok {
		log.Println("roomID missing in URL Parameters")
		return
	}

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal("Web Socket Upgrade Error", err)
	}

	AllRooms.InsertToRoom(roomID[0], false, ws)

	go broadcaster()

	for {
		var msg broadcastMsg

		err := ws.ReadJSON(&msg.Message)
		if err != nil {
			log.Fatal("Read Error: ", err)
		}

		msg.Client = ws
		msg.RoomID = roomID[0]

		log.Println(msg.Message)

		broadcast <- msg
	}
}
